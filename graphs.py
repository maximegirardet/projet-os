import matplotlib.pyplot as plt
import os
import time
from mpl_toolkits import mplot3d
import numpy as np

def measure(name, nb_args=0):
    '''
    retourne le temps d'exécution d'un programme
    name[0] : nom du programme
    name[1...] : nom des arguments
    '''
    #Pour les programmes de test sans argument
    if nb_args == 0:
        start_time = time.time()
        os.system("export LD_LIBRARY_PATH=$PWD/install/lib:$LD_LIBRARY_PATH && taskset -c 3 ./install/bin/" + name[0])
        exec_time = time.time()-start_time

    #Pour les programmes de test avec un argument (nombre de threads)
    elif nb_args == 1:
        start_time = time.time()
        os.system("export LD_LIBRARY_PATH=$PWD/install/lib:$LD_LIBRARY_PATH && taskset -c 3 ./install/bin/" + name[0] +" "+ name[1])
        exec_time = time.time()-start_time

    #Pour les programmes de test à deux arguments (nombre de threads et nombre de yield)
    elif nb_args == 2:
        start_time = time.time()
        os.system("export LD_LIBRARY_PATH=$PWD/install/lib:$LD_LIBRARY_PATH && taskset -c 3 ./install/bin/" + name[0] +" "+ name[1] +" "+ name[2])
        exec_time = time.time()-start_time
    else:
        return -1
    return exec_time


def search_test(num, files):
    '''
    retourne une liste à deux éléments contenant les noms des fichiers de test concernés
    num: identifiant du test
    files: liste de tous les fichiers de test
    '''
    result = []
    for f in files:
        if f[0:2] == num:
            result.append(f)
    if is_pthread(result[0]):
        result[0],result[1] = result[1], result[0]
    return result


def is_pthread(name):
    return name[-7:] == "pthread"

def create_graph_0_arg(names):
    '''
    crée un graphe en barres affichant les temps d'exécution des fichiers de test
    names: liste des noms des deux fichiers ayant le même identifiant 
    '''
    perf = [average([names[0]],0), average([names[1]],0)]
    plt.bar(range(2), perf)
    plt.xticks(range(2), names)
    plt.ylabel("Temps d'exécution")
    plt.savefig("./graphs/" + names[0])
    plt.close()

def average(name,  nb_args):
    '''
    retourne une moyenne de la fonction measure
    '''
    moy = 0
    for i in range(1,2):
        moy += measure(name, nb_args)
    return moy



def create_graph_1_arg(names):
    '''
    crée un graphe du temps d'exécution des programmes en fonction du nombre de threads
    names: liste des noms des deux fichiers ayant le même identifiant 
    '''
    prog0 = [names[0], "0"]
    prog1 = [names[1], "0"]
    perf0 = []
    perf1 = []
    nb_threads =  np.linspace(0, 500, 100)
    for i in nb_threads:
        prog0[1]=str(i)
        prog1[1]=str(i)
        perf0.append(average(prog0,1))
        perf1.append(average(prog1,1))
    plt.plot(nb_threads, perf0, label=names[0])
    plt.plot(nb_threads, perf1, label=names[1])
    plt.xlabel("Nombre de threads")
    plt.ylabel("Temps d'exécution")
    plt.title(names[0]+" "+names[1])
    plt.legend()
    plt.savefig("./graphs/" + names[0])
    plt.close()

def graph_fibo(names):
    '''
    crée un graphe du temps d'exécution des programmes sur la suite de fibonacci en fonction du nombre de threads
    names: liste des noms des deux fichiers ayant le même identifiant 
    '''
    prog0 = [names[0], "0"]
    prog1 = [names[1], "0"]
    perf0 = []
    perf1 = []
    plt.figure(figsize=(15,10))
    x = range(1,22)
    for i in x:
        prog0[1]=str(i)
        prog1[1]=str(i)
        perf0.append(average(prog0,1))
        perf1.append(average(prog1,1))
    plt.plot(x, perf0, label=names[0])
    plt.plot(x, perf1, label=names[1])
    plt.xlabel("x")
    plt.ylabel("Temps d'exécution")
    plt.title("Fibonacci")
    plt.legend()
    plt.savefig("./graphs/Fibonacci")
    plt.close()

def graph_tri_fusion(names):
    '''
    crée un graphe du temps d'exécution des programmes de test de tri fusion en fonction de la taille du tableau
    names: liste des noms des deux fichiers ayant le même identifiant
    '''
    prog0 = [names[0], "0"]
    prog1 = [names[1], "0"]
    perf0 = []
    perf1 = []
    plt.figure(figsize=(15,10))
    x = range(1,100)
    for i in x:
        prog0[1]=str(i)
        prog1[1]=str(i)
        perf0.append(average(prog0,1))
        perf1.append(average(prog1,1))
    plt.plot(x, perf0, label=names[0])
    plt.plot(x, perf1, label=names[1])
    plt.xlabel("taille du tableau")
    plt.ylabel("Temps d'exécution")
    plt.title("Tri-fusion")
    plt.legend()
    plt.savefig("./graphs/tri-fusion")
    plt.close()


def test_nbthread_function_nb_yield(names, start_y, stop_y, start_t, stop_t):
    '''
    crée un graphe du temps d'exécution des programmes en fonction du nombre de threads pour un nombre de yields fixé
    names: liste des noms des deux fichiers ayant le même identifiant 
    start_y, start_t : valeur de départ pour le nb de yield et de thread
    stop_y, stop_t : valeur de départ pour le nb de yield et de thread
    '''
    perf0 = []
    perf1 = []
    prog0 = [names[0], "0", "1"]
    prog1 = [names[1], "0", "1"]
    nb_yield = np.linspace(start_y, stop_y, 5)
    nb_threads = np.linspace(start_t,stop_t,10)
    colors = ['b', 'g', 'r', 'c', 'm']
    index = 0
    plt.figure(figsize=(15,10))
    for i in nb_yield:
        prog0[2]=str(i)
        prog1[2]=str(i)
        perf0 = []
        perf1 = []
        for j in nb_threads:
            prog0[1]=str(int(j))
            prog1[1]=str(int(j))
            perf0.append(average(prog0,2))
            perf1.append(average(prog1,2))
        plt.plot(nb_threads, perf0, linestyle="dashed", color=colors[index], label=str(i)+" yields "+names[0])
        plt.plot(nb_threads, perf1, color=colors[index], label=str(i)+" yields "+names[1])
        index += 1
    plt.xlabel("Nombre de threads")
    plt.ylabel("Temps d'exécution")
    plt.title(names[0] + "en fonction du nb de threads")
    plt.legend()
    plt.savefig("./graphs/" + names[0]+ "_nb_yield")
    plt.close()

def test_nbyield_function_nbthread(names, start_y, stop_y, start_t, stop_t):
    '''
    crée un graphe du temps d'exécution des programmes en fonction du nombre de yield pour un nombre de threads fixé
    names: liste des noms des deux fichiers ayant le même identifiant 
    start_y, start_t : valeur de départ pour le nb de yield et de thread
    stop_y, stop_t : valeur de départ pour le nb de yield et de thread
    '''
    perf0 = []
    perf1 = []
    prog0 = [names[0], "0", "1"]
    prog1 = [names[1], "0", "1"]
    nb_threads = np.linspace(start_t, stop_t, 5)
    nb_yield = np.linspace(start_y, stop_y, 10)
    colors = ['b', 'g', 'r', 'c', 'm']
    index = 0
    plt.figure(figsize=(15,10))
    for i in nb_threads:
        prog0[1]=str(i)
        prog1[1]=str(i)
        perf0 = []
        perf1 = []
        for j in nb_yield:
            prog0[2]=str(int(j))
            prog1[2]=str(int(j))
            perf0.append(average(prog0,2))
            perf1.append(average(prog1,2))
        plt.plot(nb_yield, perf0, linestyle="dashed", color=colors[index], label=str(i)+" threads "+names[0])
        plt.plot(nb_yield, perf1, color=colors[index], label=str(i)+" threads "+names[1])
        index += 1
    plt.xlabel("Nombre de yields")
    plt.ylabel("Temps d'exécution")
    plt.title(names[0] + "en fonction du nb de yields")
    plt.legend()
    plt.savefig("./graphs/" + names[0]) 
    plt.close()

def create_graph_2_arg(names, start_y = 0, stop_y = 100, start_t = 0, stop_t = 500):
    '''
    crée un graphe du temps d'exécution des programmes en fonction du nombre de threads et du nombre de yields
    names: liste des noms des deux fichiers ayant le même identifiant
    start_y, start_t : valeur de départ pour le nb de yield et de thread
    stop_y, stop_t : valeur de départ pour le nb de yield et de thread
    '''
    test_nbthread_function_nb_yield(names, start_y, stop_y, start_t, stop_t)
    test_nbyield_function_nbthread(names, start_y, stop_y, start_t, stop_t)


if __name__ == "__main__":
    files = [f for f in os.listdir("install/bin")]
    create_graph_0_arg(search_test("01", files))
    create_graph_0_arg(search_test("02", files))
    create_graph_0_arg(search_test("11", files))
    create_graph_0_arg(search_test("12", files))
    create_graph_1_arg(search_test("21", files))
    create_graph_1_arg(search_test("22", files))
    create_graph_1_arg(search_test("23", files))
    create_graph_2_arg(search_test("31", files), 0, 100, 0, 500)
    create_graph_2_arg(search_test("32", files), 0, 100, 0, 500)
    create_graph_2_arg(search_test("33", files), 0, 100, 0, 500)
    create_graph_1_arg(search_test("61", files))
    create_graph_1_arg(search_test("62", files))
    graph_tri_fusion(search_test("tr", files))
    graph_fibo(search_test("51", files))