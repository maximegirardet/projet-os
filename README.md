## Projet de systèmes d'exploitation

### Threads en espace utilisateur

Cette bibliothèque permet l'utilisation de *threads* en espace utilisateurs, en utilisant une interface de programmation
proche de celle de *pthreads*.

####Usage

Deux versions de ce projet sont disponibles : 

- Une version classique, présente dans le dossier `master`
- Une version disposant de la préemption, présente dans le dossier `preemption` et non aboutie à ce jour

Pour faciliter la compilation et l'exécution des tests, un *Makefile* a été écrit.

Une fois dans le dossier correspondant à la version de votre choix, plusieurs commandes sont disponibles.

- `make` compile la bibliothèque ainsi que l'ensemble des tests
- `make pthread` compile les tests en utilisant la bibliothèque `pthreads`
- `make install` copie l'intégralité des binaires dans le dossier `install`
- `make check` lance l'intégralité des tests (tests du set de tests et tests personnels)
- `make check-pthread` lance l'intégralité des tests en utilisant la bibliothèque `pthreads`
- `make valgrind` lance l'intégralité des tests en utilisant valgrind, lancé avec les options ` -v --leak-check=full --show-reachable=yes --track-origins=yes`
- `make check-[test]` lance individuellement le test spécifié (de même pour `make valgrind-[test]`)
- `make graphs` permet la création de graphes de performances, qui sont générés dans le dossier `graphs`. 
- `make clean` nettoie l'intégralité des binaires, bibliothèques dynamiques, fichiers objets et graphes 

####Auteurs

Les auteurs de ce projet sont Thomas Cauchard, Arthur Fourcadet, Maxime Girardet, Deborah Pereira et Emma Robin, élèves-ingénieurs de 2ème année à l'ENSEIRB-MATMECA.

*Equipe : S1-E1 - 12952*