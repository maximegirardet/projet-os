//
// Created by Maxime on 29/03/2021.
//

#include "tcb.h"
#include "thread.h"
#include <valgrind/valgrind.h>
#include <stdio.h>


#ifndef PROJET_OS_UTILITIES_H
#define PROJET_OS_UTILITIES_H

/**
 * Allocates and sets context stack for specified thread
 * @param thread
 * @return 0 if success, -1 on error
 */
int create_context_stack(tcb *thread);

/**
 * Wrapper used to get return value of function executed by the thread
 */
void thread_runner(void);

#endif //PROJET_OS_UTILITIES_H
