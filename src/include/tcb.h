//
// Created by Maxime on 29/03/2021.
//

#ifndef PROJET_OS_TCB_H
#define PROJET_OS_TCB_H

#include <ucontext.h>
#include "../include/queue.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

typedef struct tcb {
    pid_t tid;                //The ID of the thread
    ucontext_t context;       // The context of the thread
    int has_stack;
    unsigned valgrind_stackid;

    void *(*func)(void *);    // The func pointer to the thread function which will be executed
    void *funcarg;            //The arguments to be passed to the thread function
    void **retval;            // The return value of the function
    STAILQ_ENTRY(tcb) ready_entries; //The queue
    STAILQ_ENTRY(tcb) mutex_entries;//The queue for a mutex
    int isZombie;
    struct tcb *joiner;
} tcb;

/**
 * Creates and allocates a new tcb
 */
tcb *new_tcb(void);

/**
 * Frees the stack context and the tcb
 * @param tcb
 */
void free_tcb(tcb *tcb);

#endif //PROJET_OS_TCB_H
