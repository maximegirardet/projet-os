#ifndef __THREAD_H__
#define __THREAD_H__

#ifndef USE_PTHREAD

#include "tcb.h"
/**
 * Thread identifier
 */
typedef void *thread_t;

/**
 * Gets the identifier of the current thread.
 * @returns The identifier of the current thread.
 */
thread_t thread_self(void);

/**
 * Creates a new thread.
 * @param newthread The identifier of the new thread (allocated by user)
 * @param func The function executed by the new thread
 * @param funcarg Arguments passed to the function func
 * @return 0 on success, -1 on failure
 */
extern int thread_create(thread_t *newthread, void *(*func)(void *), void *funcarg);

/**
 * Lets another thread take control.
 * @return 0 on success, -1 on failure
 */
extern int thread_yield(void);

/**
 * Waits for a thread to terminate.
 *
 * @param thread The thread we're waiting for
 * @param retval The thread's return value is placed here. If `NULL` is passed, the return value is ignored.
 * @return 0 on success, an error number on failure.
 */
extern int thread_join(thread_t thread, void **retval);

/**
 * Terminates the calling thread and returns a value.
 *
 * This function never returns.
 * @param return_value The return value captured by thread_join
 */
extern void thread_exit(void *retval); //TODO: Add __attribute__ ((__noreturn__)) when this function is implemented

/* Interface possible pour les mutex */
typedef struct thread_mutex {
    STAILQ_HEAD(, tcb) mutex_queue;
    int lock;
} thread_mutex_t;

int thread_mutex_init(thread_mutex_t *mutex);

int thread_mutex_destroy(thread_mutex_t *mutex);

int thread_mutex_lock(thread_mutex_t *mutex);

int thread_mutex_unlock(thread_mutex_t *mutex);

#else /* USE_PTHREAD */

/* Si on compile avec -DUSE_PTHREAD, ce sont les pthreads qui sont utilisés */
#include <sched.h>
#include <pthread.h>
#define thread_t pthread_t
#define thread_self pthread_self
#define thread_create(th, func, arg) pthread_create(th, NULL, func, arg)
#define thread_yield sched_yield
#define thread_join pthread_join
#define thread_exit pthread_exit

/* Interface possible pour les mutex */
#define thread_mutex_t            pthread_mutex_t
#define thread_mutex_init(_mutex) pthread_mutex_init(_mutex, NULL)
#define thread_mutex_destroy      pthread_mutex_destroy
#define thread_mutex_lock         pthread_mutex_lock
#define thread_mutex_unlock       pthread_mutex_unlock

#endif /* USE_PTHREAD */

#endif /* __THREAD_H__ */