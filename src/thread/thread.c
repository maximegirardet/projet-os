//
// Created by Maxime on 26/03/2021.
//

#include "../include/utilities.h"

tcb *mainThread = NULL;
tcb *threadToFree = NULL;

STAILQ_HEAD(, tcb) head_ready;

// Used to start this function when the library is loaded
__attribute__((unused)) __attribute__((constructor))
static void initialize_threads() {
    STAILQ_INIT(&head_ready);
    mainThread = new_tcb();
    create_context_stack(mainThread);
    getcontext(&mainThread->context);
    mainThread->retval = NULL;
    STAILQ_INSERT_HEAD(&head_ready, mainThread, ready_entries);
}

// Used to start this function when the library is unloaded
__attribute__((unused)) __attribute__((destructor))
static void free_threads() {
    tcb *thr;
    while ((thr = STAILQ_FIRST(&head_ready)) != NULL) {
        STAILQ_REMOVE_HEAD(&head_ready, ready_entries);
        if (thr != mainThread) {
            free_tcb(thr);
        }
    }
    free_tcb(mainThread);
    if (threadToFree != NULL) {
        free_tcb(threadToFree);
    }
}

thread_t thread_self(void) {
    return STAILQ_FIRST(&head_ready);
}

int thread_create(thread_t *newthread, void *(*func)(void *), void *funcarg) {

    *newthread = new_tcb();
    tcb *new = *newthread;

    if (getcontext(&new->context) == -1) {
        return -1;
    }

    if (create_context_stack(new) == -1) {
        return -1;
    }

    makecontext(&new->context, thread_runner, 1, new->tid);

    new->func = func;
    new->funcarg = funcarg;
    STAILQ_INSERT_TAIL(&head_ready, new, ready_entries);
    return thread_yield();
}

static int only_one_thread(void) {
    return STAILQ_NEXT(STAILQ_FIRST(&head_ready), ready_entries) == NULL;
}


static int thread_yield_from(tcb *current) {

    tcb *next = thread_self();

    if (next == current) {
        return 0;
    } else {
        return swapcontext(&current->context, &next->context);
    }
}


int thread_yield(void) {
    if (only_one_thread()) {
        return 0;
    }
    tcb *current = thread_self();

    STAILQ_REMOVE_HEAD(&head_ready, ready_entries);
    STAILQ_INSERT_TAIL(&head_ready, current, ready_entries);

    return thread_yield_from(current);
}

int thread_join(thread_t thread, void **retval) {
    if (thread == NULL) {
        return -1;
    }

    tcb *thr = thread;

    if (thr->joiner != NULL) {
        printf("error join\n");
        return -1;
    }

    thr->joiner = thread_self();

    if (!thr->isZombie) {
        tcb *current = thread_self();
        STAILQ_REMOVE_HEAD(&head_ready, ready_entries);

        if (STAILQ_EMPTY(&head_ready)) {
            STAILQ_INSERT_HEAD(&head_ready, current, ready_entries);
            thr->joiner = NULL;
            return -1;
        }

        thread_yield_from(current);
    }

    if (retval != NULL) {
        *retval = thr->retval;
    }

    if (thr != mainThread) {
        free_tcb(thr);
    }

    return 0;
}

//__attribute__((__noreturn__))
void thread_exit(void *retval) {
    tcb *current = thread_self();
    current->retval = retval;
    STAILQ_REMOVE_HEAD(&head_ready, ready_entries);
    current->isZombie = 1;

    if (current->joiner != NULL) {
        STAILQ_INSERT_TAIL(&head_ready, current->joiner, ready_entries);
    }

    if (STAILQ_EMPTY(&head_ready)) {
        threadToFree = current;
        setcontext(&mainThread->context);
    } else {
        tcb *next = thread_self();
        swapcontext(&current->context, &next->context);
    }
}

/* Interface possible pour les mutex */
int thread_mutex_init(thread_mutex_t *mutex) {
    if (mutex == NULL)
        return -1;
    STAILQ_INIT(&mutex->mutex_queue);
    return 0;
}

int thread_mutex_destroy(thread_mutex_t *mutex) {
    if (mutex == NULL || mutex->lock == 1)
        return -1;
    return 0;
}

int thread_mutex_lock(thread_mutex_t *mutex) {
    if (mutex == NULL)
        return -1;
    if (mutex->lock == 1){
        tcb *current = thread_self();
        STAILQ_REMOVE_HEAD(&head_ready, ready_entries);
        STAILQ_INSERT_TAIL(&mutex->mutex_queue, current, mutex_entries);
        if (STAILQ_EMPTY(&head_ready)) {
            return -1;
        }
        thread_yield_from(current);
    }
    else
        __sync_fetch_and_add(&(mutex->lock), 1);
    return 0;
}

int thread_mutex_unlock(thread_mutex_t *mutex) {
    if (mutex == NULL || mutex->lock == 0)
        return -1;
    if (!STAILQ_EMPTY(&mutex->mutex_queue)){
        tcb *next = STAILQ_FIRST(&mutex->mutex_queue);
        (void)next;
        STAILQ_REMOVE_HEAD(&mutex->mutex_queue, mutex_entries);
        STAILQ_INSERT_TAIL(&head_ready, next, ready_entries);
    }
    else
        mutex->lock = 0;
    return 0;
}
