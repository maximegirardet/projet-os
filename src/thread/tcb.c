//
// Created by Maxime on 30/03/2021.
//

#include "../include/utilities.h"
#include <stdio.h>

tcb *new_tcb(void) {
    static pid_t next_id = 1;
    tcb *new;
    if ((new = malloc(sizeof *new)) == NULL) {
        return NULL;
    }
    new->tid = next_id++;
    new->has_stack = -1;
    new->joiner = NULL;
    new->isZombie = 0;
    return new;
}


void free_tcb(tcb *tcb) {
    if (tcb != NULL) {
        if (tcb->has_stack == 0) {
            VALGRIND_STACK_DEREGISTER(tcb->valgrind_stackid);
            free(tcb->context.uc_stack.ss_sp);
        }
        free(tcb);
    }
}
