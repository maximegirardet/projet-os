//
// Created by Maxime on 29/03/2021.
//

#include <sys/time.h>
#include <stdlib.h>
#include "../include/utilities.h"

#define STACK_SIZE (64*1024)


int create_context_stack(tcb *thread) {
    void *stack;

    if ((stack = malloc(STACK_SIZE)) == NULL) {
        return -1;
    }

    thread->context.uc_stack.ss_flags = 0;
    thread->context.uc_stack.ss_size = STACK_SIZE;
    thread->context.uc_stack.ss_sp = stack;
    thread->context.uc_link = NULL;
    thread->has_stack = 0;
    thread->valgrind_stackid = VALGRIND_STACK_REGISTER(thread->context.uc_stack.ss_sp, thread->context.uc_stack.ss_sp +
                                                                                       thread->context.uc_stack.ss_size);
    return 0;
}

void thread_runner(void) {
    tcb *this = thread_self();
    void *result = this->func(this->funcarg);

    thread_exit(result);
}

