IDIR =src/include
CC=gcc
CFLAGS= -I$(IDIR)
VFLAGS= -v --leak-check=full --show-reachable=yes --track-origins=yes

ODIR = src/obj
LDIR = build/lib
EXDIR = build/bin
SRCDIR = src/thread
TDIR = tst
GRDIR = graphs
LIBS=-lm

_DEPS = thread.h tcb.h utilities.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = thread.o tcb.o utilities.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

_TST = example tri-fusion test-join 01-main 02-switch 03-equity 11-join 12-join-main 21-create-many 22-create-many-recursive 23-create-many-once 31-switch-many 32-switch-many-join 33-switch-many-cascade 51-fibonacci 61-mutex 62-mutex 81-deadlock
TST = $(patsubst %,$(EXDIR)/%,$(_TST))
TSTPT =  $(patsubst %,$(EXDIR)/%-pthread,$(_TST))

NB_THREAD = 5
NB_YIELD = 2

all: build libthread $(TST)

build:
	mkdir -p $(LDIR) $(EXDIR) src/obj install/bin install/lib

$(ODIR)/%.o: $(SRCDIR)/%.c $(DEPS)
	$(CC) -c -Wall -Werror -fPIC -o $@ $< $(CFLAGS)

libthread: $(OBJ)
	gcc -shared -o $(LDIR)/$@.so $^

$(EXDIR)/%: $(TDIR)/%.c $(DEPS)
	$(CC) -L./$(LDIR) -o $@ $< $(CFLAGS) -lthread

check: $(TST)
	export LD_LIBRARY_PATH=./$(LDIR):$(LD_LIBRARY_PATH) && for test in $(TST); do echo \\n---- $$test -----\\n && ./$$test $(NB_THREAD) $(NB_YIELD); done

check-%: $(EXDIR)/%
	export LD_LIBRARY_PATH=./$(LDIR):$(LD_LIBRARY_PATH) && echo \\n---- $@ -----\\n && ./$< $(NB_THREAD) $(NB_YIELD)

valgrind : $(TST)
	export LD_LIBRARY_PATH=./$(LDIR):$(LD_LIBRARY_PATH) && for test in $(TST); do echo \\n---- $$test -----\\n && valgrind $(VFLAGS) ./$$test $(NB_THREAD) $(NB_YIELD); done

valgrind-% : $(EXDIR)/%
	export LD_LIBRARY_PATH=./$(LDIR):$(LD_LIBRARY_PATH) && echo \\n---- $@ -----\\n && valgrind $(VFLAGS) ./$< $(NB_THREAD) $(NB_YIELD)

$(EXDIR)/%-pthread: $(TDIR)/%.c $(DEPS)
	$(CC) -DUSE_PTHREAD -o $@ $< $(CFLAGS) -pthread

pthread: $(TSTPT)

check-pthread: $(TSTPT)
	for test in $(TSTPT); do if [ "$$test" != "$(EXDIR)/81-deadlock-pthread" ]; then echo \\n---- $$test -----\\n && ./$$test $(NB_THREAD) $(NB_YIELD); fi; done

graphs: libthread  $(TSTPT) $(TST) install
	mkdir -p graphs && python3 graphs.py

install: libthread $(TSTPT) $(TST)
	cp $(TST) $(TSTPT) install/bin/ && cp build/lib/libthread.so install/lib/

.PHONY: clean

clean:
	rm -fr $(ODIR)/*.o $(EXDIR)/* $(LDIR)/*.so *~ core $(IDIR)/*~ $(GRDIR)/* $(GRDIR) install/lib/* install/bin/*