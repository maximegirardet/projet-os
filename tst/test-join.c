#include "../src/include/thread.h"
#include <stdio.h>
#include <assert.h>

static void *threadfunc(void *arg) {
    char *name = arg;
    printf("je suis le thread %p, lancé avec l'argument %s\n",
           (void *) thread_self(), name);
    return arg;
}

static void *threadfunc2(void* arg) {
    thread_t thread2;
    void* retval2;
    int err;

    printf("je suis le thread %p lancé avec l'argument %s\n",
           (void *) thread_self(),(char *)arg);

    err = thread_create(&thread2, threadfunc, "thread2");
    assert(!err);

    printf("le thread1 a créé le thread %p\n",
           (void *) thread2);

    printf("le thread1 attend le threads 2\n");
    err=thread_join(thread2, &retval2);
    assert(!err);

    return retval2;
}

int main(int argc, char *argv[]) {
    thread_t thread1;
    void *retval1;
    int err;

    printf("le main lance 1 thread\n");
    err = thread_create(&thread1, threadfunc2, "thread1");
    assert(!err);
    printf("le main a créé le thread %p\n",
           (void *) thread1);

    printf("le main attend le thread 1\n");
    err=thread_join(thread1,&retval1);
    assert(!err);

    printf("Valeur de retour %s\n", (char*) retval1);
    return 0;
}