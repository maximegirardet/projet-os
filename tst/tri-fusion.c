#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "thread.h"
#include <assert.h>


struct params{
    int i;
    int j;
    int* tab;
    int* tmp;
};

void initialize_params(struct params* p,int i,int j,int* tab,int* tmp){
    p->i = i;
    p->j = j;
    p->tab = tab;
    p->tmp = tmp;
}

void* triFusion(void * _values) {
    thread_t th, th2;
    int err;
    struct params p1,p2;
    void *res = NULL, *res2 = NULL;
    struct params* value = _values;

    thread_yield();

    int i = value->i;
    int j = value->j;
    int* tab = value->tab;
    int* tmp = value->tmp;

    if ( j <= i ){
        return NULL;
    }
    int m = (i + j) / 2;

    initialize_params(&p1,i,m,tab,tmp);
    initialize_params(&p2,m+1,j,tab,tmp);

    err = thread_create(&th, triFusion,(void*) &p1);
    assert(!err);
    err = thread_create(&th2, triFusion,(void*) &p2);
    assert(!err);

    err = thread_join(th, &res);
    assert(!err);
    err = thread_join(th2, &res2);
    assert(!err);

    int pg = i;
    int pd = m + 1;
    int c;

    for(c = i; c <= j; c++) {
        if(pg == m + 1) {
            tmp[c] = tab[pd];
            pd++;
        }else if (pd == j + 1) {
            tmp[c] = tab[pg];
            pg++;
        }else if (tab[pg] < tab[pd]) {
            tmp[c] = tab[pg];
            pg++;
        }else {
            tmp[c] = tab[pd];
            pd++;
        }
    }

    for(c = i; c <= j; c++) {
        tab[c] = tmp[c];
    }
    return NULL;
}



int main(int argc, char* argv[]) {
    struct timeval tv1, tv2;
    int tab[100], tmp[100];
    struct params values;

    if (argc < 2){
        printf("Usage : %s taille_tab\n",argv[0]);
        return 1;
    }

    int size = atoi(argv[1]);
    srand (time(NULL));

    printf("Soit le tableau à trier: [ ");
    for (int j=0; j<size; j++){
        tab[j] = rand()%100;
        printf("%d ",tab[j]);
    }
    printf("]\n");

    initialize_params(&values,0,size-1,tab,tmp);

    gettimeofday(&tv1, NULL);
    triFusion((void*) &values);
    gettimeofday(&tv2, NULL);
    double s = (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec) * 1e-6;

    printf("\n Tableau trié : ");
    for(int i = 0; i < size; i++)  {
        printf(" %4d", tab[i]);
    }
    printf("\n");
    printf("Tri fusion de %d entier en %e s\n", size, s);
    return 0;
}